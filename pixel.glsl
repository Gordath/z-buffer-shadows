uniform sampler2DShadow shadowmap;

varying vec3 normal, vpos, ldir, spot_dir;
varying vec4 shadow_coords;

void main()
{
	vec3 texcoord = shadow_coords.xyz / shadow_coords.w;
	float stex = shadow2D(shadowmap, texcoord).x;

	vec3 n = normalize(normal);
	vec3 v = -normalize(vpos);
	vec3 l = normalize(ldir);
	vec3 h = normalize(v + l);

	float ndotl = max(dot(n, l), 0.0);
	float ndoth = max(dot(n, h), 0.0);

	vec3 ambient = gl_FrontMaterial.ambient.rgb * gl_LightModel.ambient.rgb;
	vec3 diffuse = gl_FrontMaterial.diffuse.rgb * ndotl;
	vec3 specular = gl_FrontMaterial.specular.rgb * pow(ndoth, gl_FrontMaterial.shininess);

	float spot_dot = max(dot(-l, normalize(spot_dir)), 0.0);
	float limit = gl_LightSource[0].spotCosCutoff;
	float spot = smoothstep(0.0, 0.05, spot_dot - limit);

	gl_FragColor.rgb = ambient + (diffuse + specular) * spot * stex;
	gl_FragColor.a = 1.0;
}
