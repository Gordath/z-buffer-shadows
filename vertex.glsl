varying vec3 normal, vpos, ldir, spot_dir;
varying vec4 shadow_coords;

void main()
{
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	vpos = (gl_ModelViewMatrix * gl_Vertex).xyz;
	normal = gl_NormalMatrix * gl_Normal;

	ldir = gl_LightSource[0].position.xyz - vpos;
	spot_dir = gl_LightSource[0].spotDirection;

	mat4 offset_mat = mat4(0.5, 0.0, 0.0, 0.0,
							0.0, 0.5, 0.0, 0.0,
							0.0, 0.0, 0.5, 0.0,
							0.5, 0.5, 0.5, 1.0);

	mat4 shadow_matrix = offset_mat * gl_TextureMatrix[0];

	shadow_coords = shadow_matrix * gl_Vertex;
}
