src = $(wildcard src/*.cpp)
csrc = $(wildcard src/*.c)
obj = $(src:.cpp=.o) $(csrc:.c=.o)
dep = $(obj:.o=.d)
bin = zshad

CXXFLAGS = -pedantic -Wall -g
LDFLAGS = $(libgl) -lm

ifeq ($(shell uname -s), Darwin)
	libgl = -framework OpenGL -framework GLUT -lGLEW
else
	libgl = -lGL -lGLU -lglut -lGLEW
endif

$(bin): $(obj)
	$(CXX) -o $@ $(obj) $(LDFLAGS)

-include $(dep)

.cpp.d:
	@$(CPP) $(CXXFLAGS) $< -MM -MT $(@:.d=.o) >$@

.PHONY: clean
clean:
	rm -f $(obj) $(bin) $(dep)
