#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "opengl.h"
#include "light.h"
#include "sdr.h"
#include "vec.h"

static bool init();
static void idle();
static void depth_pass(float sec);
static void render_pass(float sec);
static void display();
static void draw_scene(float sec);
static void draw_rtex();
static void set_material(const Vector3 &kd, const Vector3 &ks, float shin);
static void reshape(int x, int y);
static void keyboard(unsigned char key, int x, int y);
static void mouse(int bn, int state, int x, int y);
static void motion(int x, int y);


// camera parameters
static float cam_theta, cam_phi = 20, cam_dist = 22;

// framebuffer width and height
static int width, height;
// render target texture width and height
static int sbuf_size = 1024;

static unsigned int fbo;	// framebuffer object
static unsigned int rtex;	// texture render-target

static SpotLight light;

static unsigned int sdrprog;	// shader program
static unsigned int dummy_renderbuffer;


int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitWindowSize(1024, 768);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	glutCreateWindow("render to texture");

	glutIdleFunc(idle);
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);

	if(!init()) {
		return 1;
	}

	glutMainLoop();
	return 0;
}

static bool init()
{
	glewInit();

	if(!(sdrprog = create_program_load("vertex.glsl", "pixel.glsl"))) {
		return false;
	}

	width = glutGet(GLUT_WINDOW_WIDTH);
	height = glutGet(GLUT_WINDOW_HEIGHT);
	printf("framebuffer size: %dx%d\n", width, height);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_POINT_SMOOTH);
	glPointSize(3.0);
	glLineWidth(3.0);


	glGenTextures(1, &rtex);
	glBindTexture(GL_TEXTURE_2D, rtex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, sbuf_size, sbuf_size, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);

	// create framebuffer object
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, rtex, 0);

	glGenRenderbuffers(1, &dummy_renderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, dummy_renderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, sbuf_size, sbuf_size);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, dummy_renderbuffer);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	light.set_position(-6, 8, 6);
	light.set_target(0, 0, 0);
	light.set_fov(40.0);

	return true;
}

static void idle()
{
	glutPostRedisplay();
}

static void display()
{
	float sec = (float)glutGet(GLUT_ELAPSED_TIME) / 1000.0;

	light.set_position(sin(sec) * 8, 8, 0);

	// step 1: draw from the viewpoint of the light into the depth buffer texture
	depth_pass(sec);

	// show the depth buffer
	/*glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindTexture(GL_TEXTURE_2D, rtex);
	draw_rtex();*/

	// step 2: project the depth buffer texture and do the depth comparison
	render_pass(sec);

	glutSwapBuffers();
	assert(glGetError() == GL_NO_ERROR);
}

// XXX this function performs the depth-buffer rendering pass
static void depth_pass(float sec)
{
	glPolygonOffset(2, 1);
	glEnable(GL_POLYGON_OFFSET_FILL);

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// setup the light projection matrix to render with the field of view of the light
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluPerspective(light.get_fov() * 2.0, 1.0, 0.5, 100.0);

	// change the viewport to match the render-target size
	glViewport(0, 0, sbuf_size, sbuf_size);

	// setup the light view matrix to render from the viewpoint of the light
	Vector3 lpos = light.get_position();
	Vector3 ltarg = light.get_target();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	gluLookAt(lpos.x, lpos.y, lpos.z, ltarg.x, ltarg.y, ltarg.z, 0, 1, 0);

	// disable writing to the color buffer
	glColorMask(0, 0, 0, 0);

	draw_scene(sec);

	glColorMask(1, 1, 1, 1);

	// restore the original modelview/projection matrices
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	// restore the original viewport
	glViewport(0, 0, width, height);

	// return to the window render target
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glDisable(GL_POLYGON_OFFSET_FILL);
}

static void render_pass(float sec)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// setup the view matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0, 0, -cam_dist);
	glRotatef(cam_phi, 1, 0, 0);
	glRotatef(cam_theta, 0, 1, 0);

	light.setup(0);

	// setup the texture matrix to project the depth buffer image onto the scene
	// from the point of view of the light
	Vector3 lpos = light.get_position();
	Vector3 ltarg = light.get_target();

	// let's put the LIGHT's (view * projection) matrix into GL_TEXTURE
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
	gluPerspective(light.get_fov() * 2.0, 1.0, 0.5, 100.0);
	gluLookAt(lpos.x, lpos.y, lpos.z, ltarg.x, ltarg.y, ltarg.z, 0, 1, 0);

	// bind the depth texture
	glBindTexture(GL_TEXTURE_2D, rtex);
	glUseProgram(sdrprog);

	draw_scene(sec);

	glUseProgram(0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();

	// also draw the light
	glPointSize(3.0);
	glBegin(GL_POINTS);
	glColor3f(1.0, 0.95, 0.2);
	glVertex3f(lpos.x, lpos.y, lpos.z);
	glColor3f(1, 1, 1);
	glEnd();
}

static void draw_scene(float sec)
{
	glMatrixMode(GL_MODELVIEW);

	// --- draw the teapot ---
	set_material(Vector3(1.0, 0.4, 0.2), Vector3(0.8, 0.8, 0.8), 60.0);

	glFrontFace(GL_CW);
	glutSolidTeapot(1.0);
	glFrontFace(GL_CCW);

	// --- draw the ground ---
	set_material(Vector3(0.2, 0.8, 0.3), Vector3(0.2, 0.2, 0.2), 20.0);

	float y = -0.71;
	glBegin(GL_QUADS);
	glNormal3f(0, 1, 0);
	glVertex3f(-60, y, 60);
	glVertex3f(60, y, 60);
	glVertex3f(60, y, -60);
	glVertex3f(-60, y, -60);
	glEnd();

	// --- draw the torus (which has a non-identity world matrix) ---

	// set the world matrix in both modelview (camera's) and texture (light's) slots
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(0, -0.5, 0);
	glRotatef(sec * 50.0, 0, 1, 0);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glTranslatef(0, -0.5, 0);
	glRotatef(sec * 50.0, 0, 1, 0);

	set_material(Vector3(0.2, 0.4, 0.8), Vector3(0.4, 0.4, 0.4), 40.0);
	glutSolidTorus(0.5, 4.0, 12, 32);

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glUseProgram(0);
}

static void draw_rtex()
{
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, rtex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);

	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex2f(-1, -1);
	glTexCoord2f(1, 0);
	glVertex2f(1, -1);
	glTexCoord2f(1, 1);
	glVertex2f(1, 1);
	glTexCoord2f(0, 1);
	glVertex2f(-1, 1);
	glEnd();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

static void set_material(const Vector3 &kd, const Vector3 &ks, float shin)
{
	float diff[] = {kd.x, kd.y, kd.z, 1.0};
	float spec[] = {ks.x, ks.y, ks.z, 1.0};
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, diff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shin);
}

static void reshape(int x, int y)
{
	glViewport(0, 0, x, y);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, (float)x / (float)y, 0.5, 500.0);
}

static void keyboard(unsigned char key, int x, int y)
{
	switch(key) {
	case 27:
		exit(0);

	default:
		break;
	}
}

static bool bnstate[32];
static int prev_x, prev_y;

static void mouse(int bn, int state, int x, int y)
{
	prev_x = x;
	prev_y = y;

	bnstate[bn - GLUT_LEFT_BUTTON] = state == GLUT_DOWN;
}

static void motion(int x, int y)
{
	int dx = x - prev_x;
	int dy = y - prev_y;
	prev_x = x;
	prev_y = y;

	if(bnstate[0]) {
		cam_theta += dx * 0.5;
		cam_phi += dy * 0.5;

		if(cam_phi < -90) cam_phi = -90;
		if(cam_phi > 90) cam_phi = 90;
		glutPostRedisplay();
	}
	if(bnstate[2]) {
		cam_dist += dy * 0.1;

		if(cam_dist < 0) cam_dist = 0;
		glutPostRedisplay();
	}
}
