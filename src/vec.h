#ifndef VEC_H_
#define VEC_H_

class Vector3 {
public:
	float x, y, z;

	Vector3();
	Vector3(float x, float y, float z);

	Vector3 operator -() const;

	float length() const;

	void normalize();
};

Vector3 operator +(const Vector3 &a, const Vector3 &b);
Vector3 operator -(const Vector3 &a, const Vector3 &b);
Vector3 operator *(const Vector3 &a, float s);
Vector3 operator /(const Vector3 &a, float s);

float dot(const Vector3 &a, const Vector3 &b);
Vector3 cross(const Vector3 &a, const Vector3 &b);

Vector3 reflect(const Vector3 &v, const Vector3 &n);

#endif	// VEC_H_
