#ifndef OPENGL_H_
#define OPENGL_H_

#ifdef __APPLE__
/* macos */
#include <GL/glew.h>
#include <GLUT/glut.h>
#elif defined(WIN32)
/* windows (assuming we have set the visual studio include paths up) */
#include "glew.h"
#include "glut.h"
#else
/* everything else */
#include <GL/glew.h>
#include <GL/glut.h>
#endif

#endif	/* OPENGL_H_ */
