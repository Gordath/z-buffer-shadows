#ifndef LIGHT_H_
#define LIGHT_H_

#include "vec.h"

class Light {
private:
	Vector3 pos;

public:
	Light();
	Light(float x, float y, float z);

	virtual void set_position(float x, float y, float z);
	virtual void set_position(const Vector3 &pos);
	virtual const Vector3 &get_position() const;

	virtual void setup(int idx) const;
};

class SpotLight : public Light {
private:
	Vector3 target;
	float fov;

public:
	SpotLight();
	SpotLight(float x, float y, float z);

	virtual void set_fov(float fov);
	virtual float get_fov() const;

	virtual void set_target(float x, float y, float z);
	virtual void set_target(const Vector3 &targ);
	virtual const Vector3 &get_target() const;

	virtual void setup(int idx) const;
};

#endif	// LIGHT_H_
