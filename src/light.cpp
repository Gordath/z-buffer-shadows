#define _USE_MATH_DEFINES
#include "light.h"
#include "opengl.h"

Light::Light()
{
}

Light::Light(float x, float y, float z)
	: pos(x, y, z)
{
}

void Light::set_position(float x, float y, float z)
{
	pos = Vector3(x, y, z);
}

void Light::set_position(const Vector3 &pos)
{
	this->pos = pos;
}

const Vector3 &Light::get_position() const
{
	return pos;
}

void Light::setup(int idx) const
{
	float lpos[4] = {pos.x, pos.y, pos.z, 1.0};
	glLightfv(GL_LIGHT0 + idx, GL_POSITION, lpos);
}


// spotlight implementation
SpotLight::SpotLight()
{
	set_fov(50.0);
}

SpotLight::SpotLight(float x, float y, float z)
	: Light(x, y, z)
{
}

void SpotLight::set_fov(float fov)
{
	this->fov = fov;
}

float SpotLight::get_fov() const
{
	return fov;
}

void SpotLight::set_target(float x, float y, float z)
{
	target = Vector3(x, y, z);
}

void SpotLight::set_target(const Vector3 &targ)
{
	target = targ;
}

const Vector3 &SpotLight::get_target() const
{
	return target;
}

void SpotLight::setup(int idx) const
{
	Light::setup(idx);

	Vector3 dir = target - get_position();
	dir.normalize();

	glLightfv(GL_LIGHT0 + idx, GL_SPOT_DIRECTION, &dir.x);

	glLightf(GL_LIGHT0 + idx, GL_SPOT_CUTOFF, fov);
}
